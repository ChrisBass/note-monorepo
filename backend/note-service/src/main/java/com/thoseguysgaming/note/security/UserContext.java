package com.thoseguysgaming.note.security;

import org.springframework.stereotype.Component;

@Component
public class UserContext {

    private final ThreadLocal<String> currentUserHeader = new ThreadLocal<>();

    void setCurrentUserHeader(String authHeader){
        currentUserHeader.set(authHeader);
    }

    public String getCaller(){
        return currentUserHeader.get();
    }
}
