package com.thoseguysgaming.note.entities;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class ResponseDtoList<T> {
    private List<T> data;
}
