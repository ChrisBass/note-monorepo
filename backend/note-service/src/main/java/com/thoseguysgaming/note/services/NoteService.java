package com.thoseguysgaming.note.services;

import com.thoseguysgaming.note.entities.note.Note;
import com.thoseguysgaming.note.entities.note.NoteRequestDto;
import com.thoseguysgaming.note.entities.note.NoteResponseDto;
import com.thoseguysgaming.note.exception.NotFoundException;
import com.thoseguysgaming.note.repository.NoteRepository;
import com.thoseguysgaming.note.security.UserContext;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
public class NoteService {

    private final NoteRepository noteRepository;
    private final UserContext userContext;
    private final UserClient userClient;

    public List<NoteResponseDto> getNotes(){
        return noteRepository.findAllByBelongsTo(getCallingId())
            .stream().map(NoteResponseDto::from).toList();
    }

    public NoteResponseDto updateNote(UUID noteId, NoteRequestDto request){
        Note edit = noteRepository.findNoteByIdAndBelongsTo(noteId, getCallingId())
            .orElseThrow(() -> new NotFoundException("Note not found."));
        edit.setMessage(request.getMessage());
        edit.setImportant(request.isImportant());
        return NoteResponseDto.from(noteRepository.save(edit));
    }

    public NoteResponseDto deleteNote(UUID noteId){
        Note delete = noteRepository.findNoteByIdAndBelongsTo(noteId, getCallingId())
            .orElseThrow(() -> new NotFoundException("Note not found."));
        noteRepository.delete(delete);
        return NoteResponseDto.from(delete);
    }

    public NoteResponseDto createNote(NoteRequestDto request){
        Note create = request.toNote();
        create.setBelongsTo(getCallingId());
        Note saved = noteRepository.save(create);
        return NoteResponseDto.from(saved);
    }

    /*
        Contact user-service to get the calling user's id
     */
    private UUID getCallingId(){
        return userClient.getUser(userContext.getCaller()).getId();
    }
}
