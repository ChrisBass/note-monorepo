package com.thoseguysgaming.note.repository;

import com.thoseguysgaming.note.entities.note.Note;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface NoteRepository extends JpaRepository<Note, UUID> {

    List<Note> findAllByBelongsTo(UUID belongsTo);
    Optional<Note> findNoteByIdAndBelongsTo(UUID id, UUID belongsTo);
}
