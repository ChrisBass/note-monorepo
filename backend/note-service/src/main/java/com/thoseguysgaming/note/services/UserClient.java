package com.thoseguysgaming.note.services;

import com.thoseguysgaming.note.entities.user.UserResponseDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient("user-service")
public interface UserClient {

    @GetMapping(value="/api/me")
    UserResponseDto getUser(@RequestHeader("Authorization") String authHeader);
}
