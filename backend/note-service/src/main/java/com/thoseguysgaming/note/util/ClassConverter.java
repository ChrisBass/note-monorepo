package com.thoseguysgaming.note.util;

import org.modelmapper.ModelMapper;

public class ClassConverter {

    private ClassConverter(){

    }

    private static final ModelMapper mapper = createModelMapper();

    private static ModelMapper createModelMapper(){
        ModelMapper mapper = new ModelMapper();
        //do any configuration of mapper here.
        return mapper;
    }

    public static <TSource, TTarget> TTarget convert(final TSource sourceObject, final Class<TTarget> targetClass){
        return mapper.map(sourceObject, targetClass);
    }
}
