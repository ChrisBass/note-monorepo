package com.thoseguysgaming.note.controllers;

import com.thoseguysgaming.note.entities.ResponseDtoList;
import com.thoseguysgaming.note.entities.note.NoteRequestDto;
import com.thoseguysgaming.note.entities.note.NoteResponseDto;
import com.thoseguysgaming.note.entities.user.UserResponseDto;
import com.thoseguysgaming.note.services.NoteService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("/api")
@AllArgsConstructor
public class NoteController {

    private final NoteService noteService;

    @GetMapping
    public ResponseDtoList<NoteResponseDto> getNotes(){
        return new ResponseDtoList<>(noteService.getNotes());
    }

    @PostMapping
    public NoteResponseDto createNote(@RequestBody NoteRequestDto request){
        return noteService.createNote(request);
    }

    @PutMapping("/{id}")
    public NoteResponseDto editNote(@PathVariable("id") UUID editId, @RequestBody NoteRequestDto request){
        return noteService.updateNote(editId, request);
    }

    @DeleteMapping("/{id}")
    public NoteResponseDto deleteNote(@PathVariable("id") UUID deleteId){
        return noteService.deleteNote(deleteId);
    }

}
