package com.thoseguysgaming.note.entities.note;

import com.thoseguysgaming.note.util.ClassConverter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class NoteRequestDto {
    private String message;
    private boolean important;
    private OffsetDateTime createdAt;

    public Note toNote(){
        return ClassConverter.convert(this, Note.class);
    }
}
