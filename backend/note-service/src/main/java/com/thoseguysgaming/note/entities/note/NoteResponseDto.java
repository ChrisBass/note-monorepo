package com.thoseguysgaming.note.entities.note;

import com.thoseguysgaming.note.util.ClassConverter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class NoteResponseDto {
    private UUID id;
    private String message;
    private boolean important;
    private OffsetDateTime createdAt;
    private OffsetDateTime updatedAt;

    public static NoteResponseDto from(Note entity) {
        return ClassConverter.convert(entity, NoteResponseDto.class);
    }
}
