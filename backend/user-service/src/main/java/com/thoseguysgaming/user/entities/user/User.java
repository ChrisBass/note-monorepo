package com.thoseguysgaming.user.entities.user;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.validator.constraints.UniqueElements;

import java.time.OffsetDateTime;
import java.util.Map;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "tgg_users")
@Builder
public class User {

    public User(Map<String, Object> tokenAttributes){
        this.sub = tokenAttributes.get("sub").toString();
        this.username = tokenAttributes.get("preferred_username").toString();
    }

    @Id
    @GeneratedValue
    private UUID id;

    @Size(min = 1, max = 255)
    @Column(unique = true)
    private String sub;

    @Size(min = 1, max = 64)
    private String username;

    @CreationTimestamp
    private OffsetDateTime createdAt;
}
