package com.thoseguysgaming.user.services;

import com.thoseguysgaming.user.entities.user.User;
import com.thoseguysgaming.user.entities.user.UserResponseDto;
import com.thoseguysgaming.user.repository.UserRepository;
import com.thoseguysgaming.user.util.ClassConverter;
import lombok.AllArgsConstructor;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;

@Service
@AllArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    public UserResponseDto getOrCreateUserFromAuthToken(JwtAuthenticationToken token){

        Map<String, Object> attributes = token.getTokenAttributes();
        Optional<User> existing = userRepository.findBySub(attributes.get("sub").toString());
        if(existing.isPresent()){
            return ClassConverter.convert(existing.get(), UserResponseDto.class);
        }
        User user = new User(attributes);
        userRepository.save(user);
        return ClassConverter.convert(user, UserResponseDto.class);
    }
}
