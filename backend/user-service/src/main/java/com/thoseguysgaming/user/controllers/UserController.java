package com.thoseguysgaming.user.controllers;

import com.thoseguysgaming.user.entities.user.UserResponseDto;
import com.thoseguysgaming.user.services.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
@AllArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping("/me")
    public ResponseEntity<UserResponseDto> getRequestingUser(JwtAuthenticationToken authToken){
        System.out.println("Called /api/me");
        return new ResponseEntity<>(userService.getOrCreateUserFromAuthToken(authToken), HttpStatus.OK);
    }
}
