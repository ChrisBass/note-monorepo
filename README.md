# Note Web App

## Prerequisites

1. install [pnpm](https://pnpm.io/installation)
2. install [direnv](https://direnv.net/)
3. install java 21

## Dev Quickstart

1. load .envrc: `direnv allow .`
2. start a docker compose: 
   1. `docker compose -f <composeFile> build`
   2. `docker compose -f <composeFile> up`
      1. `docker-compose.yml` - runs all containers for the app
      2. `docker-compose-dev.yml` - runs all dependency containers (no microservices or ui)
      3. `no-frontend.yml` - runs everything except for the ui
3. import realm config in keycloak
4. [create a test user](https://www.keycloak.org/docs/latest/server_admin/#proc-creating-user_server_administration_guide) in the realm `thoseguys-dev`

### Spinning up the backend locally (running docker-compose-dev.yml):
1. This is useful for making backend changes and having the servers update on the fly.
2. start the user service (with live reload): from `./backend/user-service`
   1. in first terminal start the live reload listener:
      1. `./gradlew clean assemble`
      2. `./gradlew -t bootJar`
   2. in another terminal start the server:
      1`./gradlew bootRun`
3. start the note service (with live reload): from `./backend/note-service`
   1. in first terminal start the live reload listener:
      1. `./gradlew clean assemble`
      2. `./gradlew -t bootJar`
   2. in another terminal start the server:
      1`./gradlew bootRun`

### Spinning up the ui locally (running docker-compose-dev.yml or no-frontend.yml):
1. start the frontend: from `./frontend/ui` 
   1. `pnpm i`
   2. `pnpm start dev`


## Useful information:

### ports: 
| application      | port |
|------------------|------|
| note-service     | 8080 |
| user-service     | 8082 |
| api-gateway      | 8090 |
| discovery-server | 8761 |
| keycloak         | 8081 |
| pgAdmin          | 8888 |
| ui               | 3000 |

### Keycloak login:
| username | password |
|----------|----------|
| admin    | Pa55w0rd |

### PgAdmin login:
| user                     | password        |
|--------------------------|-----------------|
| test@thoseguysgaming.com | strong-password |

### pgAdmin connections:
| db      | host    | port | user          | password       |
|---------|---------|------|---------------|----------------|
| note-db | note-db | 5432 | noteUser      | noteAdmin      |
| user-db | user-db | 5432 | thoseGuysUser | thoseGuysAdmin |