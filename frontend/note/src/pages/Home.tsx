import {Grid, IconButton, Paper, Theme, Typography} from "@mui/material";
import {makeStyles} from "@mui/styles";
import {useAuth} from "react-oidc-context";
import {useGetNotesQuery} from "@thoseguys/state-management/src/services/apiClient.ts";
import {AddCircle} from "@mui/icons-material";
import {useState} from "react";
import AddNoteForm from "../components/forms/AddNoteForm.tsx";
import { Note } from "@thoseguys/state-management/src/types/notes.ts";
import NoteCard from "../components/NoteCard.tsx";
import EditNoteForm from "../components/forms/EditNoteForm.tsx";
import DeleteNoteDialog from "../components/dialogs/DeleteNoteDialog.tsx";

const useStyle = makeStyles((theme: Theme) => ({
  page: {
    position: "relative",
    width: '100%',
    height: '94%',
    margin: '0'
  },
  textCentered: {
    textAlign: 'center',
  },
  wrapper: {
    position: 'relative',
    width: '75%',
    left: '50%',
    height: '100%',
    transform: 'translateX(-50%)',
    marginTop: '20px'
  },
  toolbar: {
    backgroundColor: theme.palette.primary.light,
    justifyContent: 'flex-end',
  },
  noAuth: {
    position: 'relative',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)'
  }
}));
const Home = () => {
  const auth = useAuth();
  const getNotesResponse = useGetNotesQuery(auth.user?.access_token);
  const [addNoteOpen, setAddNoteOpen] = useState(false);
  const [editNoteOpen, setEditNoteOpen] = useState(false);
  const [deleteNoteOpen, setDeleteNoteOpen] = useState(false);
  const [actionNote, setActionNote] = useState<Note|undefined>(undefined);
  const classes = useStyle();

  const buildEdit = (note: Note) => (() => {
    setActionNote(note);
    setEditNoteOpen(true);
  });

  const buildDelete = (note: Note) => (() => {
    setActionNote(note);
    setDeleteNoteOpen(true);
  });

  if (getNotesResponse.error) {
    console.log(getNotesResponse.error)
  }

  return (
    <Paper className={classes.page}>
      {auth.isAuthenticated && <Typography variant={'h1'} className={classes.textCentered}>My Notes</Typography>}
      <div className={classes.wrapper}>
        { auth.isAuthenticated ?
          <Grid container item sm={12} spacing={2} direction={'row'}>
            <Grid container className={classes.toolbar}>
              <IconButton onClick={() => setAddNoteOpen(true)}>
                <AddCircle titleAccess={'Add new note'}/>
              </IconButton>
            </Grid>
            {getNotesResponse.data?.length === 0 ?
              <Grid item sm={12}>
                <Typography variant={'body1'} className={classes.textCentered}>Nothing to see here.</Typography>
              </Grid>
              :
              <Grid container item sm={12} spacing={2}>
                {getNotesResponse.data?.map((note: Note) => <NoteCard note={note} openEdit={buildEdit(note)} openDelete={buildDelete(note)} key={note.id}/>)}
              </Grid>
            }
          </Grid>
        :
          <Paper className={classes.noAuth}>
            <Typography variant={'h3'} className={classes.textCentered}>Log in to see your notes.</Typography>
          </Paper>
        }
      </div>
      <AddNoteForm open={addNoteOpen} setOpen={setAddNoteOpen}/>
      <EditNoteForm open={editNoteOpen} setOpen={setEditNoteOpen} note={actionNote}/>
      <DeleteNoteDialog open={deleteNoteOpen} setOpen={setDeleteNoteOpen} note={actionNote}/>
    </Paper>);
}

export default Home;