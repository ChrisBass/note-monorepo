import {Button, Dialog, DialogActions, DialogContent, DialogTitle, Grid, Typography} from "@mui/material";
import {createStyles, makeStyles} from "@mui/styles";
import { useDeleteNoteMutation } from "@thoseguys/state-management/src/services/apiClient";
import {DeleteNote, Note} from "@thoseguys/state-management/src/types/notes";
import {useAuth} from "react-oidc-context";

interface Props{
  open: boolean;
  setOpen: (open: boolean) => void;
  note?: Note;
}

const useStyle = makeStyles(() =>
  createStyles({
    warning: {
      fontWeight: 'bold',
      textAlign: 'center',
      color: 'red'
    },
  })
);

const DeleteNoteDialog = ({open, setOpen, note}: Props) => {
  const [deleteNote, result] = useDeleteNoteMutation();
  const auth = useAuth();
  const classes = useStyle();

  if(!note){
    return null;
  }
  const handleDelete = async () => {
    const deleteRequest: DeleteNote = {
      id: note.id,
      auth: auth.user?.access_token,
    }
    await deleteNote(deleteRequest);
    console.log(result);
    setOpen(false);
  };

  return(
    <Dialog open={open} onClose={() => setOpen(false)}>
      <DialogTitle>Confirm Delete</DialogTitle>
      <DialogContent>
        <Grid container>
          <Grid item sm={12}>
            <Typography variant={'subtitle1'} className={classes.warning}>This action cannot be undone.</Typography>
          </Grid>
          <Grid item sm={4} md={3}>
            <label htmlFor={'message'}>Message:</label>
          </Grid>
          <Grid item sm={8} md={9}>
            <Typography variant={'body1'}>{note.message}</Typography>
          </Grid>
          { note.important &&
          <Grid item sm={12}>
            <Typography variant={'subtitle1'} className={classes.warning}>This message is important</Typography>
          </Grid>
          }
        </Grid>
      </DialogContent>
      <DialogActions>
        <Button onClick={() => setOpen(false)} color="error" variant={'outlined'}>
          Cancel
        </Button>
        <Button type="submit" color="primary" onClick={handleDelete}>
          Confirm
        </Button>
      </DialogActions>
    </Dialog>
  )
}

export default DeleteNoteDialog;