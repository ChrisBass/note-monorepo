import {FieldValues, useController, UseControllerProps} from "react-hook-form";
import {Grid, GridProps, TextField, TextFieldProps} from "@mui/material";
import {capitalize} from "../../util.ts";

interface Props<TFieldValues extends FieldValues> extends UseControllerProps<TFieldValues>{
  gridProps?: GridProps;
  fieldProps?: TextFieldProps;
  label: string;
}

const GenericTextField = <TFieldValues extends FieldValues>(
  {
    name,
    label,
    control,
    gridProps,
    fieldProps,
    rules = {
      required: `${capitalize(name)} is required`
    },
  }: Props<TFieldValues>) => {

  const {
    field,
    fieldState: {error},
  } = useController({
    name,
    control,
    rules
  })
  return (
    <Grid item {...gridProps}>
      <TextField
        {...fieldProps}
        label={label}
        value={field.value}
        inputRef={field.ref}
        onChange={field.onChange}
        onBlur={field.onBlur}
        error={Boolean(error)}
        helperText={error?.message}
        fullWidth
        margin="normal"
      />
    </Grid>
  );
}

export default GenericTextField;