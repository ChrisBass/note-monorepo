import {useEditNoteMutation} from "@thoseguys/state-management/src/services/apiClient";
import {Button, Checkbox, Drawer, Grid, Theme, Typography} from "@mui/material";
import {makeStyles} from "@mui/styles";
import {useForm} from "react-hook-form";
import {Note, NoteRequest, PutNote} from "@thoseguys/state-management/src/types/notes";
import {useAuth} from "react-oidc-context";
import GenericTextField from "./GenericTextField.tsx";
import {useEffect} from "react";

interface Props{
  open: boolean;
  setOpen: (open: boolean) => void;
  note?: Note;
}

const useStyle = makeStyles((theme: Theme) => ({
  form: {
    backgroundColor: theme.palette.secondary.light,
    position: "relative",
    width: '400px',
    height: '100%'
  },
  centered: {
    textAlign: "center"
  },
  gridContainer: {
    position: "relative",
    margin: "10px",
    top: "50%",
    transform: "translateY(-50%)"
  },
  fieldWrapper: {
    position: "relative",
    width: "94%",
    justifyContent: 'center',
  },
  formActions: {
    justifyContent: 'center',
  },
  formFields:{
    justifyContent: 'center'
  }
}));

const EditNoteForm = ({open, setOpen, note}: Props) => {
  const [editNote, result] = useEditNoteMutation();
  const classes = useStyle();
  const auth = useAuth();
  const formDefaults: NoteRequest = {
    message: note?.message || '',
    important: note?.important || false
  };
  const {
    handleSubmit,
    control,
    reset,
    register,
  } = useForm<NoteRequest>({
    defaultValues: formDefaults
  });

  useEffect(() => {
    const values: NoteRequest = {
      message: note?.message || '',
      important: note?.important || false
    }
    reset(values);
  }, [reset, note]);


  if(!note){
    return null;
  }

  const submit = async (data: NoteRequest) => {
    const request: PutNote = {
      auth: auth.user?.access_token,
      body: data,
      id: note.id
    }
    await editNote(request);
    console.log(result);
    setOpen(false);
  }

  const handleCancel = () => {
    reset(formDefaults);
    setOpen(false);
  }

  return(
    <Drawer
      open={open}
      onClose={() => setOpen(false)}
      anchor={'right'}>
      <div className={classes.form}>
        <Grid container className={classes.gridContainer}>
          <Grid item>
            <Typography className={classes.centered} variant={'h3'}>Edit note</Typography>
          </Grid>
          <Grid container item className={classes.formFields} sm={12}>
            <div className={classes.fieldWrapper}>
              <form onSubmit={handleSubmit(submit)}>
                <GenericTextField label={'Message: '}
                                  name={'message'}
                                  control={control}
                                  gridProps={{xs: 12}}
                                  fieldProps={{multiline: true}}
                />
                <Grid item>
                  <Checkbox id={'importantCheckbox'} {...register("important")} defaultChecked={note.important}/>
                  <label htmlFor={'importantCheckbox'}>Important</label>
                </Grid>

                <Grid container item className={classes.formActions}>
                  <Grid item>
                    <Button color="error" variant={"outlined"} onClick={handleCancel}>
                      Cancel
                    </Button>
                  </Grid>
                  <Grid item>
                    <Button type="submit" color="primary">
                      Save
                    </Button>
                  </Grid>
                </Grid>
              </form>
            </div>
          </Grid>
        </Grid>
      </div>
    </Drawer>
  );
}

export default EditNoteForm;