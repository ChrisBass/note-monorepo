import {Card, Grid, IconButton, Theme, Typography} from "@mui/material";
import {makeStyles} from "@mui/styles";
import {Note} from "@thoseguys/state-management/src/types/notes.ts";
import {Delete, EditNote, PriorityHigh} from "@mui/icons-material";

const useStyle = makeStyles((theme: Theme) => ({
  colored: {
    position: "relative",
    width: "100%",
    height: "100%",
    backgroundColor: theme.palette.primary.light
  },
  card: {
    minHeight: "150px",
    height: "150px",
  },
  textContainer: {
    position: 'absolute',
    width: '75%',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)'
  },
  icon: {
    color: 'red',
    position: 'absolute',
    right: '0',
  },
  cardActions:{
    position: "absolute",
    bottom: '0',
    right: '0',
    justifyContent: 'flex-end'
  },
  actionIcon: {

  }
}));

interface Props {
  note: Note;
  openEdit: () => void;
  openDelete: () => void;
}

const NoteCard = ({note, openEdit, openDelete}: Props) => {
  const classes = useStyle();
  return (
    <Grid item xs={12} md={6} lg={4} xl={3}>
      <Card className={classes.card}>
        <div className={classes.colored}>
          {note.important && <PriorityHigh className={classes.icon} titleAccess={'IMPORTANT'}/>}
          <div className={classes.textContainer}>
            <Typography variant={'body1'}>{note.message}</Typography>
          </div>
          <div className={classes.cardActions}>
            <IconButton className={classes.actionIcon} onClick={openDelete}>
              <Delete titleAccess={'Delete note'} />
            </IconButton>
            <IconButton className={classes.actionIcon} onClick={openEdit}>
              <EditNote titleAccess={'Edit note'} className={classes.actionIcon}/>
            </IconButton>
          </div>
        </div>
      </Card>
    </Grid>
  );
}

export default NoteCard;