import {Card, Typography} from "@mui/material";

interface ErrorProps{
  message: string
}
const ErrorLayout = ({message}: ErrorProps) => {
  return (
    <Card>
      <Typography variant={'h1'}>Error:</Typography>
      <Typography variant={'body1'}>{message}</Typography>
    </Card>
  );
}

export default ErrorLayout;