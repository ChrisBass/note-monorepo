import {Outlet} from "react-router-dom";
import {makeStyles} from "@mui/styles";
import NotesAppBar from "../components/NotesAppBar.tsx";

const useStyle = makeStyles(() => ({
  app: {
    width: '100vw',
    height: '100vh',
    margin: '0'
  }
}));
const BaseLayout = () => {
  const classes = useStyle();
  return (
    <div className={classes.app}>
      <NotesAppBar/>
      <Outlet/>
    </div>
  );
}

export default BaseLayout;