import {BrowserRouter, Navigate, Outlet, Route, Routes} from "react-router-dom";
import Home from "@thoseguys/note/src/pages/Home.tsx";
import {Typography} from "@mui/material";
import {useAuth} from "react-oidc-context";
import BaseLayout from "../layouts/BaseLayout.tsx";

const PrivateOutlet = ()  => {
  const auth = useAuth();
  return auth.isAuthenticated ? <Outlet/> : <Navigate to="/" />;
}

const Router = () => {
  return (
    <BrowserRouter basename={''}>
      <Routes>
        <Route path="/" element={<BaseLayout/>}>
          <Route path="/" element={<Home/>}/>
          <Route path="/profile" element={<PrivateOutlet/>}>
            <Route path={''} element={<Typography variant={'h3'}>Private</Typography>}/>
          </Route>
          <Route path={'/public'} element={<Typography variant={'h3'}>Public</Typography>}/>
        </Route>
      </Routes>
    </BrowserRouter>
  )
};
export default Router;