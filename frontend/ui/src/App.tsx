import {useAuth} from "react-oidc-context";
import ErrorLayout from "./layouts/ErrorLayout.tsx";
import Router from "./routing/Router.tsx";
function App() {

  const auth = useAuth();

  switch (auth.activeNavigator) {
    case "signinSilent":
      return <div>Signing you in ...</div>;
    case "signoutRedirect":
      return <div>Signing you out ...</div>;
  }

  if (auth.isLoading) {
    return <div>Auth is loading...</div>;
  }

  if (auth.error) {
    return <ErrorLayout message={auth.error.message}/>;
  }

  return <Router/>
}

export default App
