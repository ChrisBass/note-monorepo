import React from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'
import App from "./App.tsx";
import {AuthProvider} from "react-oidc-context";
import {Provider} from "react-redux";
import {store} from '@thoseguys/state-management/src/store.ts';
import {ThemeProvider} from "@mui/material";
import {theme} from "./theme/Theme.ts";

const oidcConfig =  {
  authority: `${import.meta.env.VITE_KEYCLOAK_URL}/auth/realms/${import.meta.env.VITE_KEYCLOAK_REALM}`,
  client_id: import.meta.env.VITE_CLIENT_ID,
  redirect_uri: import.meta.env.VITE_BASE_URL,
  post_logout_redirect_uri: import.meta.env.VITE_BASE_URL,
  onSigninCallback: (): void => {
    window.history.replaceState({}, document.title, window.location.pathname);
  },
  revokeTokensOnSignout: true,
  frontchannel_logout_supported: true,
}

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <AuthProvider {...oidcConfig}>
      <Provider store={store}>
        <ThemeProvider theme={theme}>
        <App/>
        </ThemeProvider>
      </Provider>
    </AuthProvider>
  </React.StrictMode>,
)
