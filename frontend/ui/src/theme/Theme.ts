import {createTheme} from "@mui/material"

export const theme = createTheme({
  palette: {
    primary: {
      main: '#1434A4'
    },
    secondary: {
      main: '#F28C28'
    }
  },

});