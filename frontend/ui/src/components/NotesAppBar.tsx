import {AppBar, Grid, IconButton, Menu, MenuItem, Toolbar, Typography, useMediaQuery, useTheme} from "@mui/material";
import {AccountCircle, Login} from "@mui/icons-material";
import {makeStyles} from "@mui/styles";
import {useState} from "react";
import {Link} from "react-router-dom";
import {useAuth} from "react-oidc-context";
import {useGetMeQuery} from "@thoseguys/state-management/src/services/apiClient";


const useStyle = makeStyles(() => ({
  textField: {
    left: "50%",
    transform: 'translateX(-50%)'
  },
  header: {
    textAlign: "center"
  },
  link: {
    textDecoration: "none",
  },
  homeLink: {
    textDecoration: 'none',
    color: 'white'
  },
  menuText: {
    color: "rgba(175,175,175,0.8)"
  },
  gridContainer: {
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  appbar: {
    height: '6%'
  }
}));

const NotesAppBar = () => {
  const classes = useStyle();
  const theme = useTheme();
  const isLargeScreen = useMediaQuery(theme.breakpoints.up('md'));
  const [anchor, setAnchor] = useState<null | HTMLElement>(null);
  const open = Boolean(anchor);
  const auth = useAuth();
  const meResponse = useGetMeQuery(auth.user?.access_token);

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchor(event.currentTarget);
  }
  const handleClose = () => {
    setAnchor(null);
  }

  const logInHandler = async () => {
    await auth.clearStaleState();
    await auth.signinRedirect();
  }

  const logOutHandler = async () => {
    await auth.revokeTokens();
    await auth.removeUser();
    await auth.clearStaleState();
    await auth.signoutRedirect();
  }

  return (
    <AppBar position={'static'} className={classes.appbar}>
      <Toolbar>
        <Link to={'/'} className={classes.homeLink}>
          <Typography variant={isLargeScreen ? 'h3' : 'h4'}>Notes</Typography>
        </Link>
        <Grid container className={classes.gridContainer} spacing={2}>
          {auth.isAuthenticated ?
            <>
              <Grid item>
                <Typography variant={'body1'}>{meResponse.data?.username}</Typography>
              </Grid>
              <Grid item>
                <IconButton
                  size="large"
                  edge="end"
                  aria-label="My account"
                  onClick={handleClick}
                >
                  <AccountCircle/>
                </IconButton>
              </Grid>
            </>
            :
            <Grid item>
              <IconButton
                size="large"
                edge="end"
                aria-label="Log in"
                onClick={logInHandler}
              >
                <Login/>
              </IconButton>
            </Grid>
          }
        </Grid>
      </Toolbar>
      {auth.isAuthenticated &&
          <Menu open={open} anchorEl={anchor} data-testid={'appbar-menu'}
                onClose={handleClose}>
              <MenuItem onClick={logOutHandler} className={classes.menuText}>Logout</MenuItem>
          </Menu>
      }

    </AppBar>
  );
}

export default NotesAppBar;