import {createApi, fetchBaseQuery} from "@reduxjs/toolkit/query/react";
import {User} from "../types/user";
import {DeleteNote, Note, noteEmptyTag, PostNote, PutNote} from "../types/notes";

export interface Response{
  message: string
}

export const apiClient = createApi({
  reducerPath: 'notes',
  baseQuery: fetchBaseQuery(
    {
      baseUrl: `${import.meta.env.VITE_API_URL}`
    }),
  tagTypes: ['Note'],
  endpoints: (builder) => ({
    getNotes: builder.query<Note[], string|undefined>({
      query: (token: string|undefined) => ({
        url: 'note/api',
        headers: getHeaders(token, false)
      }),
      transformResponse: (response: {data: Note[]}) => response.data,
      providesTags: (result)  => result ? [
        ...result.map(({id}) => ({type: 'Note' as const, id})),
        noteEmptyTag
      ] : [
        noteEmptyTag
      ]
    }),
    createNote: builder.mutation<Note, PostNote>({
      query: (request: PostNote) => ({
        url: 'note/api',
        method: 'POST',
        headers: getHeaders(request.auth, true),
        body: JSON.stringify(request.body)
      }),
      invalidatesTags: [noteEmptyTag]
    }),
    editNote: builder.mutation<Note, PutNote>({
      query: (request: PutNote) => ({
        url: `note/api/${request.id}`,
        method: 'PUT',
        headers: getHeaders(request.auth, true),
        body: JSON.stringify(request.body)
      }),
      invalidatesTags: [noteEmptyTag]
    }),
    deleteNote: builder.mutation<Note, DeleteNote>({
      query: (request: DeleteNote) => ({
        url: `note/api/${request.id}`,
        method: 'DELETE',
        headers: getHeaders(request.auth, false)
      }),
      invalidatesTags: [noteEmptyTag]
    }),
    getMe: builder.query<User, string|undefined>({
      query: (token: string|undefined) => ({
        url: 'user/api/me',
        headers: getHeaders(token, false)
      })
    })
  })
})

export const getHeaders = (token: string|undefined, withBody: boolean) => {
  if(token){
    return withBody ? {
      'Authorization': `Bearer ${token}`,
      'Content-Type': 'application/json'
    } : {
      'Authorization': `Bearer ${token}`
    }
  }
  return {}
}

export const {useGetNotesQuery, useCreateNoteMutation, useEditNoteMutation, useDeleteNoteMutation, useGetMeQuery} = apiClient;