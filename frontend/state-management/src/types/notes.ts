import {DateTime} from "luxon";

export interface Note{
  id: string
  message: string;
  important: boolean;
  createdAt: DateTime;
  updatedAt?: DateTime;
}

export type NoteRequest = Omit<Note, 'id' | 'createdAt' | 'updatedAt'>

export interface PostNote{
  auth?: string,
  body: NoteRequest
}

export interface PutNote extends PostNote{
  id: string
}

export type DeleteNote = Omit<PutNote, 'body'>

export interface NoteTag{
  type: 'Note'
  id: string
}

export const noteEmptyTag: NoteTag = {
  type: 'Note',
  id: 'TAG',
}